CC=gcc
CFLAGS=-c -g

lista: lista_enlazada_bugs.o
	$(CC) -o lista  lista_enlazada_bugs.o

lista_enlazada_bugs.o: lista_enlazada_bugs.c
	$(CC) $(CFLAGS) lista_enlazada_bugs.c

clean:
	rm *o lista
